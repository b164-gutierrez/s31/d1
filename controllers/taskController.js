const Task = require('../models/task');


// controll function for getting all the task.

module.exports.getAllTasks = () =>{
	return Task.find({}).then( result => {
		return result;
	})
};

// ===============================================================

// Activity
// 2. Create a controller function for retrieving a specific task.
// 3. Return the result back to the client/Postman.
// 4. Process a GET request at the "/tasks/:id" route using postman to get a specific task.


module.exports.getStatus = (id) => {
	return Task.findById(id).then((result, err) => {
		if(err) {
			console.log(err);
			return false;
		} else {
			return result;
		}
	})
}

// 5. Status 'Complete'



module.exports.statusUpdate = (taskId, newStatus) => {
	return Task.findByIdAndUpdate(taskId).then((result, error) => {
		if(error){
			console.log(error);
			return false;
		} 

		result.status = newStatus.status; 

		return result.save().then((newStatus, saveErr) => {
			if(saveErr){
				console.log(saveErr);
				return false;
			} else {
				return newStatus;
			}
		})

	})
}



// ===============================================================


// Creating task
module.exports.createTask = (requestBody) => {

	// create object
	let newTask = new Task({
		name: requestBody.name
	})

	return newTask.save().then((task, error) => {
		if (error) {
			console.log(error);
			return false;
		} else {
			return task;
		}
	})
}


// Deleting a task
// 'id' url parameter passed from the taskRoute.js

module.exports.deleteTask = (taskId) => {
	return Task.findByIdAndRemove(taskId).then((removedTask, err) => {
		if(err) {
			console.log(err);
			return false;
		} else {
			return removedTask;
		}
	})
}




// Update a task
module.exports.updateTask = (taskId, newContent) => {
	return Task.findById(taskId).then((result, error) => {
		if(error){
			console.log(error);
			return false;
		} 

		result.name = newContent.name; 

		return result.save().then((updatedTask, saveErr) => {
			if(saveErr){
				console.log(saveErr);
				return false;
			} else {
				return updatedTask;
			}
		})

	})
}