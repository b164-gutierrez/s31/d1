const express = require('express');
const router = express.Router();

const TaskController = require('../controllers/taskController');

// Get all tasks
router.get('/', (req, res) => {
	TaskController.getAllTasks().then(resultFromController => res.send(resultFromController))
})










// ======================================================
// activity
router.get('/:id', (req, res) => {
	TaskController.getStatus(req.params.id, req.body).then(result => res.send(result));
})



// Create a 'Complete' task
router.put('/:id', (req, res) => {
	TaskController.statusUpdate(req.params.id, req.body).then(result => res.send(result));
})

// ======================================================










// Create a task
router.post('/', (req, res) => {
	TaskController.createTask(req.body).then(result => res.send(result));
})



// Delete a task
// URL 'http://localhost:3001/tasks/:id'
// params parameter
// :id = wildcard
router.delete('/:id', (req, res) => {
	TaskController.deleteTask(req.params.id).then(result => res.send(result));
})




// Update a task
router.put('/:id', (req, res) => {
	TaskController.updateTask(req.params.id, req.body).then(result => res.send(result));
})





module.exports = router;