// setup dependencies
const express = require('express');
const mongoose = require('mongoose');


// This allows us to use all the routes defined in 'taskRoute.js'
const taskRoute = require('./routes/taskRoute');


// server setup
const app = express();
const port = 3001;
app.use(express.json());
app.use(express.urlencoded({extended:true}));

// database connection
mongoose.connect('mongodb+srv://beear:Barryjosh08@cluster0.hnwmk.mongodb.net/batch164_to-do?retryWrites=true&w=majority', 
	{
		useNewUrlParser: true,
		useUnifiedTopology: true
	});

let db = mongoose.connection;

// connection error message.
db.on('error', console.error.bind(console, 'connection error'));

// connection successful message.
db.once('open', () => console.log("We're connected to the cloud database"));



// Routes
app.use('/tasks', taskRoute);
// http://localhost:3001/tasks/
app.use('/tasks/complete', taskRoute);





app.listen(port, () => console.log(`Now listening to port ${port}`));

/*
	Notes:
	models > controllers > routes > index.js
*/





































